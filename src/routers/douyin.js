/*
 * @Author: @Nice
 * @LastEditTime: 2022-12-19 18:08:04
 * @FilePath: /douyin/src/routers/douyin.js
 */

const router = require('koa-router')()
// UserContoller
const UserContoller = require('../controllers/index')

router.get('/douyin', UserContoller.Tiktok_analysis)

module.exports = router
