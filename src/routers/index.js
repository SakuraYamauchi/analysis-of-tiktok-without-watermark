const router = require('koa-router')()
const douyin = require('./douyin')
// 设置路由前缀
router.prefix('/api')
// 路由
router.use(douyin.routes(), douyin.allowedMethods())
// 导出路由
module.exports = router
