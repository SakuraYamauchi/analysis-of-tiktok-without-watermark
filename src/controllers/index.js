/*
 * @Author: @Nice
 * @LastEditTime: 2022-12-19 18:08:16
 * @FilePath: /douyin/src/controllers/index.js
 */
const UserService = require('../services/index')
const UserContoller = {
	async Tiktok_analysis(ctx, next) {
		const { url } = ctx.query
		if (!url) {
			ctx.body = {
				code: 400,
				msg: '请填写视频链接'
			}
			return
		}
		const regUrl = url.match(
			/http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/g
		)
		if (!regUrl) {
			ctx.body = {
				code: 401,
				msg: '视频链接有误'
			}
			return
		}
		const data = await UserService.Tiktok_analysis(regUrl)
		ctx.body = data
	}
}
module.exports = UserContoller
