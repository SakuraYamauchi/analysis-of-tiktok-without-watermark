/*
 * @Author: @Nice
 * @LastEditTime: 2022-12-19 18:00:24
 * @FilePath: /douyin/src/services/index.js
 */

const UserService = {
	Tiktok_analysis: async regUrl => {
		const resID = await fetch(regUrl)
		const id = resID.url.split('video/')[1].split('?')[0]
		const resInfo = await (
			await fetch(
				`https://www.iesdouyin.com/web/api/v2/aweme/iteminfo/?item_ids=${id}`
			)
		).json()
		const uri = resInfo.item_list[0].video.play_addr.uri
		const video_url = `https://aweme.snssdk.com/aweme/v1/play/?video_id=${uri}&ratio=4096p&line=0`
		const play_url = await fetch(video_url)
		const data = {
			code: 200,
			msg: 'success',
			aweme_id: resInfo.item_list[0].aweme_id,
			desc: resInfo.item_list[0].desc,
			create_time: resInfo.item_list[0].create_time,
			video: play_url.url
		}
		return data
	}
}
module.exports = UserService
