<!--
 * @Author: @Nice
 * @LastEditTime: 2022-12-19 19:40:44
 * @FilePath: /analysis-of-tiktok-without-watermark/README.md
-->
# Analysis of Tiktok without watermark

#### 介绍
Node.js 抖音去水印解析，仅用于学习交流研究使用，不可用作商业用途

#### 软件架构
Node.js v18+

#### 安装教程

1.  npm install 或者 cnpm install
2.  npm run start


#### 使用说明

1.  启动后访问 http://localhost:3003
2.  接口地址 http://localhost:3003/api/douyin?url=抖音分享链接

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 联系我

博客地址：[https://www.4z1.cn](https://www.4z1.cn)

邮箱：[Info@4z1.cn](mailto:Info@4z1.cn)




