/*
 * @Author: @Nice
 * @LastEditTime: 2022-12-19 19:06:46
 * @FilePath: /analysis-of-tiktok-without-watermark/app.js
 */
const Koa = require('koa')
const app = new Koa()
const PROT = 3003
const cors = require('koa2-cors')
const router = require('./src/routers')

app.use(async (ctx, next) => {
	try {
		await next()
	} catch (err) {
		ctx.status
		ctx.body = {
			code: 500,
			msg: err.message
		}
	}
})
app.use(router.routes()).use(router.allowedMethods())
app.use(cors)
app.listen(PROT, () => {
	console.log(`Server is running at http://localhost:${PROT}`)
})
